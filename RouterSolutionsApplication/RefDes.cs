﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RouterSolutionsApplication
{
    public class RefDes
    {
        public String RefDesKey { get; set; }
        public String RefDesSide { get; set; }
        public int PartID { get; set; }
    }
}
