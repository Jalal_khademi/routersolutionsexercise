﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using System.Text.RegularExpressions;

namespace RouterSolutionsApplication
{
    public partial class Form1 : Form
    {
        Session session;
        List<Part> parts;
        int currentPos;
        int currentRefDesPos;
        PartHelper.PartController partController;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadParts();
            map();
            setButtonsEnable();
            partController = PartHelper.PartController.create("Parts.mdb");
            partController.PartChangedEvent += new PartHelper.PartController.PartChangedEventEventHandler(refreshData);
            lblExternalUpdate.ForeColor = lblExternalUpdate.BackColor;
        }

        void refreshData(object sender , int changed)
        {
            this.Invoke((MethodInvoker)delegate
            {
                LoadParts();
                map();
                setButtonsEnable();
                lblExternalUpdate.ForeColor = Color.Black;
                timer1.Start();
            });
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if(btnNext.Text == "Add")
            {
                currentPos++;
                setButtonsEnable();
            }
            else if(btnNext.Text == "Save")
            {
                if (savePart())
                {
                    LoadParts();
                    map();
                    setButtonsEnable();
                }

            }
            else
            {
                currentPos++;
                map();
                setButtonsEnable();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            currentPos--;
            map();
            setButtonsEnable();
        }

        private void btnNextRefDes_Click(object sender, EventArgs e)
        {
            if (btnNextRefDes.Text == "Add")
            {
                currentRefDesPos++;
                setButtonsEnable();
            }
            else if (btnNextRefDes.Text == "Save")
            {
                saveRefDes();
            }
            else
            {
                currentRefDesPos++;
                mapRefDes();
                setButtonsEnable();
            }
        }

        private void saveRefDes()
        {
            if(!refDesValidation(txtRefDes.Text))
            {
                MessageBox.Show("Incorrect RefDes format or duplicate RefDes");
            }
            else
            {
                RefDes refDes = new RefDes();
                refDes.RefDesKey = txtRefDes.Text;
                refDes.RefDesSide = partController.getRefDesSide(refDes.RefDesKey) == 0 ? "top" : "bottom";
                refDes.PartID = parts[currentPos].Id;
                session.insertRefDes(refDes);
                LoadParts();
                partController.DoPartChangedEvent(parts[currentPos].Id);
                map();
                setButtonsEnable();
            }
        }

        private void btnPreRefDes_Click(object sender, EventArgs e)
        {
            currentRefDesPos--;
            mapRefDes();
            setButtonsEnable();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (!isNumeric(txtQuantity.Text))
            {
                LoadParts();
                map();
                MessageBox.Show("Quantity should be a number");
            }
            else
            {
                parts[currentPos].Description = txtDiscription.Text;
                parts[currentPos].Quantity = int.Parse(txtQuantity.Text);
                parts[currentPos].EditedAt = DateTime.Now;
                parts[currentPos].EditedBy = txtUserName.Text;
                session.updatePart(parts[currentPos]);
                partController.DoPartChangedEvent(parts[currentPos].Id);
                LoadParts();
                map();
                MessageBox.Show("Updated successfully");
            }
        }

        private void btnEditRefDes_Click(object sender, EventArgs e)
        {
            if (!refDesValidation(txtRefDes.Text))
            {
                LoadParts();
                map();
                MessageBox.Show("Incorrect RefDes format or duplicate RefDes");
            }
            else
            {
                String RefDesSide = partController.getRefDesSide(txtRefDes.Text) == 0 ? "top" : "bottom";
                session.updateRefDes(parts[currentPos].RefDeses[currentRefDesPos], txtRefDes.Text,RefDesSide);
                parts[currentPos].EditedAt = DateTime.Now;
                parts[currentPos].EditedBy = txtUserName.Text;
                session.updatePartEditRefDes(parts[currentPos]);
                partController.DoPartChangedEvent(parts[currentPos].Id);
                LoadParts();
                map();
                setButtonsEnable();
            }
        }

        private bool savePart()
        {
            if(isNumeric(txtQuantity.Text))
            {
                Part p = new Part();
                p.Description = txtDiscription.Text;
                p.Quantity = int.Parse(txtQuantity.Text);
                p.CreatedBy = txtUserName.Text;
                p.CreatedAt = DateTime.Now;
                session.insertPart(p);
                LoadParts();
                map();
                setButtonsEnable();
                partController.DoPartChangedEvent(parts[currentPos].Id);
                return true;
            }
            else
            {
                MessageBox.Show("invalid input");
                return false;
            }
        }

        private void setButtonsEnable()
        {
            if (currentPos == parts.Count - 1)
            {
                btnNext.Text = "Add";
                groupBox1.Enabled = true;
                btnEdit.Enabled = true;
            }
            else if (currentPos == parts.Count)
            {
                btnNext.Text = "Save";
                setAddGui();
            }
            else
                btnNext.Text = "Next";
            if(groupBox1.Enabled)
            {
                if (currentRefDesPos == parts[currentPos].RefDeses.Count - 1)
                {
                    btnNextRefDes.Text = "Add";
                    btnEditRefDes.Enabled = true;
                }
                else if (currentRefDesPos == parts[currentPos].RefDeses.Count)
                {
                    setAddRedDesGui();
                    btnNextRefDes.Text = "Save";
                }
                else
                btnNextRefDes.Text = ">>";
            }
            btnBack.Enabled = currentPos != 0;
            btnPreRefDes.Enabled = currentRefDesPos != 0;
        }

        private void setAddGui()
        {
            lblRId.Text = "";
            txtDiscription.Text = "";
            txtQuantity.Text = "";
            lblRCreatedAt.Text = "";
            lblRCreatedBy.Text = "";
            lblREditedAt.Text = "";
            lblREditedBy.Text = "";
            btnEdit.Enabled = false;
            groupBox1.Enabled = false;
        }

        private void setAddRedDesGui()
        {
            txtRefDes.Text = "";
            lblRPageOnTheBoard.Text = "";
            btnEditRefDes.Enabled = false;
        }

        private void map()
        {
            lblRId.Text = parts[currentPos].Id.ToString();
            txtDiscription.Text = parts[currentPos].Description.ToString();
            txtQuantity.Text = parts[currentPos].Quantity.ToString();
            lblREditedBy.Text = parts[currentPos].EditedBy.ToString();
            lblREditedAt.Text = parts[currentPos].EditedAt.ToString();
            lblRCreatedAt.Text = parts[currentPos].CreatedAt.ToString();
            lblRCreatedBy.Text = parts[currentPos].CreatedBy.ToString();
            currentRefDesPos = 0;
            mapRefDes();
        }

        private void mapRefDes()
        {
            if (parts[currentPos].RefDeses.Count > 0)
            {
                txtRefDes.Text = parts[currentPos].RefDeses[currentRefDesPos].RefDesKey;
                lblRPageOnTheBoard.Text = parts[currentPos].RefDeses[currentRefDesPos].RefDesSide;
            }
            else
            {
                txtRefDes.Text = "";
                lblRPageOnTheBoard.Text = "";
            }
        }

        private void LoadParts()
        {
            if (session == null)
            {
                session = new Session();
                currentPos = 0;
            }
            parts = session.getAllParts();
        }

        private bool refDesValidation(String refDes)
        {
            if(!String.IsNullOrEmpty(refDes))
            {
                String part1 = refDes[0].ToString();
                String part2 = refDes.Substring(1);
                return isLetter(part1) && isNumeric(part2) && !session.isRefDesExist(refDes);
            }
            return false;
        }

        private bool isNumeric(String input)
        {
            return Regex.IsMatch(input, @"^[0-9]+$");
        }

        private bool isLetter(String input)
        {
            return Regex.IsMatch(input, @"^[a-zA-Z]+$");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int fadingSpeed = 10;
            lblExternalUpdate.ForeColor = Color.FromArgb(lblExternalUpdate.ForeColor.R + fadingSpeed,
                lblExternalUpdate.ForeColor.G + fadingSpeed,
                lblExternalUpdate.ForeColor.B + fadingSpeed);

            if (lblExternalUpdate.ForeColor.R >= this.BackColor.R)
            {
                timer1.Stop();
                lblExternalUpdate.ForeColor = this.BackColor;
            }
        }
    }
}
