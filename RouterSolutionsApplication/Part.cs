﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RouterSolutionsApplication
{
    public class Part
    {
        public int Id { get; set; }
        public String Description { get; set; }
        public int Quantity { get; set; }
        public String CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public String EditedBy { get; set; }
        public DateTime EditedAt { get; set; }
        public List<RefDes> RefDeses { get; set; }

    }
}
