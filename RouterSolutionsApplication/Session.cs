﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace RouterSolutionsApplication
{
    public class Session
    {
        OleDbConnection connection;
        OleDbCommand command;
        private void ConnectTo()
        {
            connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|Parts.mdb");
            command = connection.CreateCommand();
        }
        public Session()
        {
            if(connection == null)
                ConnectTo();
        }

        private List<RefDes> getAllRefDeses(int PartId)
        {
            try
            {
                List<RefDes> res = new List<RefDes>();
                command.CommandText = "SELECT * from RefDes where PartId = " + PartId;
                command.CommandType = System.Data.CommandType.Text;
                OleDbDataReader reader = null;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    RefDes r = new RefDes();
                    r.RefDesKey = reader["RefDes"].ToString();
                    r.RefDesSide = reader["RefDesSide"].ToString();
                    r.PartID = int.Parse(reader["PartId"].ToString());
                    res.Add(r);
                }
                reader.Close();
                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void insertPart(Part p)
        {
            try
            {
                command.CommandText = "insert into Part ([Description],[Quantity],[CreatedBy],[CreatedAt])"
                + " values('" +p.Description+ "'," +p.Quantity+ ",'" +p.CreatedBy+ "','" +p.CreatedAt+ "')";

                command.CommandType = System.Data.CommandType.Text;
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        public void insertRefDes(RefDes refdes)
        {
            try
            {
                command.CommandText = "insert into RefDes ([RefDes],[RefDesSide],[PartId])"
                + " values('" + refdes.RefDesKey + "','" + refdes.RefDesSide + "'," + refdes.PartID + ")";

                command.CommandType = System.Data.CommandType.Text;
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        public List<Part> getAllParts()
        {
            try
            {
                List<Part> res = new List<Part>();
                command.CommandText = "SELECT * from Part";
                command.CommandType = System.Data.CommandType.Text;
                connection.Open();
                OleDbDataReader reader = null;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Part p = new Part();
                    p.Id = int.Parse(reader["Id"].ToString());
                    p.Description = reader["Description"].ToString();
                    p.Quantity = int.Parse(reader["Quantity"].ToString());
                    p.CreatedBy = reader["CreatedBy"].ToString();
                    if (!String.IsNullOrEmpty(reader["CreatedAt"].ToString()))
                        p.CreatedAt = DateTime.Parse(reader["CreatedAt"].ToString());
                    p.EditedBy = reader["EditedBy"].ToString();
                    if (!String.IsNullOrEmpty(reader["EditedAt"].ToString()))
                        p.EditedAt = DateTime.Parse(reader["EditedAt"].ToString());
                    res.Add(p);
                }
                reader.Close();
                foreach(Part p in res)
                {
                    p.RefDeses = getAllRefDeses(p.Id);
                }
                return res;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        public void updatePart(Part part)
        {
            try
            {
                command.CommandText = "UPDATE Part  SET Description = '" + part.Description +
                "' , Quantity = " + part.Quantity + ", EditedBy = '" + part.EditedBy +
                "' , EditedAt = '" + part.EditedAt +
                "' WHERE Id = " + part.Id + ";";

                command.CommandType = System.Data.CommandType.Text;
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        public void updatePartEditRefDes(Part part)
        {
            try
            {
                command.CommandText = "UPDATE Part  SET EditedBy = '" + part.EditedBy +
                "' , EditedAt = '" + part.EditedAt +
                "' WHERE Id = " + part.Id + ";";

                command.CommandType = System.Data.CommandType.Text;
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        public bool isRefDesExist(String refDes)
        {
            try
            {
                command.CommandText = "SELECT * from RefDes";
                command.CommandType = System.Data.CommandType.Text;
                connection.Open();
                OleDbDataReader reader = null;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["RefDes"].ToString().ToLower() == refDes.ToLower())
                        return true;
                }
                reader.Close();
                return false;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        public void updateRefDes(RefDes refDes, String newRefDes, String side)
        {
            try
            {
                command.CommandText = "UPDATE RefDes  SET RefDes = '" + newRefDes +
                    "' , PartId = " + refDes.PartID +
                    " , RefDesSide = '" + side +
                "' WHERE RefDes = '" + refDes.RefDesKey + "'";

                command.CommandType = System.Data.CommandType.Text;
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }
    }
}
