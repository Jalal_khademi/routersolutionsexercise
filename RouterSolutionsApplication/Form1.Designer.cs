﻿namespace RouterSolutionsApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblId = new System.Windows.Forms.Label();
            this.txtDiscription = new System.Windows.Forms.TextBox();
            this.lblDisctiption = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.lblCreatedBy = new System.Windows.Forms.Label();
            this.lblCreatedAt = new System.Windows.Forms.Label();
            this.lblEditedBy = new System.Windows.Forms.Label();
            this.lblEditedAt = new System.Windows.Forms.Label();
            this.lblRefDes = new System.Windows.Forms.Label();
            this.lblPageOnTheBoard = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.txtRefDes = new System.Windows.Forms.TextBox();
            this.lblRId = new System.Windows.Forms.Label();
            this.lblRCreatedBy = new System.Windows.Forms.Label();
            this.lblRCreatedAt = new System.Windows.Forms.Label();
            this.lblREditedBy = new System.Windows.Forms.Label();
            this.lblREditedAt = new System.Windows.Forms.Label();
            this.lblRPageOnTheBoard = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPreRefDes = new System.Windows.Forms.Button();
            this.btnNextRefDes = new System.Windows.Forms.Button();
            this.btnEditRefDes = new System.Windows.Forms.Button();
            this.lblUserName = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblExternalUpdate = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(25, 398);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 0;
            this.btnBack.Text = "Previous";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(34, 72);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(24, 13);
            this.lblId.TabIndex = 1;
            this.lblId.Text = "ID: ";
            // 
            // txtDiscription
            // 
            this.txtDiscription.Location = new System.Drawing.Point(149, 92);
            this.txtDiscription.Name = "txtDiscription";
            this.txtDiscription.Size = new System.Drawing.Size(100, 20);
            this.txtDiscription.TabIndex = 2;
            // 
            // lblDisctiption
            // 
            this.lblDisctiption.AutoSize = true;
            this.lblDisctiption.Location = new System.Drawing.Point(34, 95);
            this.lblDisctiption.Name = "lblDisctiption";
            this.lblDisctiption.Size = new System.Drawing.Size(62, 13);
            this.lblDisctiption.TabIndex = 3;
            this.lblDisctiption.Text = "Disctiption: ";
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Location = new System.Drawing.Point(34, 125);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(52, 13);
            this.lblQuantity.TabIndex = 4;
            this.lblQuantity.Text = "Quantity: ";
            // 
            // lblCreatedBy
            // 
            this.lblCreatedBy.AutoSize = true;
            this.lblCreatedBy.Location = new System.Drawing.Point(34, 150);
            this.lblCreatedBy.Name = "lblCreatedBy";
            this.lblCreatedBy.Size = new System.Drawing.Size(64, 13);
            this.lblCreatedBy.TabIndex = 5;
            this.lblCreatedBy.Text = "Created by: ";
            // 
            // lblCreatedAt
            // 
            this.lblCreatedAt.AutoSize = true;
            this.lblCreatedAt.Location = new System.Drawing.Point(34, 179);
            this.lblCreatedAt.Name = "lblCreatedAt";
            this.lblCreatedAt.Size = new System.Drawing.Size(62, 13);
            this.lblCreatedAt.TabIndex = 6;
            this.lblCreatedAt.Text = "Created at: ";
            // 
            // lblEditedBy
            // 
            this.lblEditedBy.AutoSize = true;
            this.lblEditedBy.Location = new System.Drawing.Point(34, 207);
            this.lblEditedBy.Name = "lblEditedBy";
            this.lblEditedBy.Size = new System.Drawing.Size(57, 13);
            this.lblEditedBy.TabIndex = 7;
            this.lblEditedBy.Text = "Edited by: ";
            // 
            // lblEditedAt
            // 
            this.lblEditedAt.AutoSize = true;
            this.lblEditedAt.Location = new System.Drawing.Point(34, 233);
            this.lblEditedAt.Name = "lblEditedAt";
            this.lblEditedAt.Size = new System.Drawing.Size(55, 13);
            this.lblEditedAt.TabIndex = 8;
            this.lblEditedAt.Text = "Edited at: ";
            // 
            // lblRefDes
            // 
            this.lblRefDes.AutoSize = true;
            this.lblRefDes.Location = new System.Drawing.Point(9, 31);
            this.lblRefDes.Name = "lblRefDes";
            this.lblRefDes.Size = new System.Drawing.Size(109, 13);
            this.lblRefDes.TabIndex = 9;
            this.lblRefDes.Text = "Reference designator";
            // 
            // lblPageOnTheBoard
            // 
            this.lblPageOnTheBoard.AutoSize = true;
            this.lblPageOnTheBoard.Location = new System.Drawing.Point(9, 56);
            this.lblPageOnTheBoard.Name = "lblPageOnTheBoard";
            this.lblPageOnTheBoard.Size = new System.Drawing.Size(101, 13);
            this.lblPageOnTheBoard.TabIndex = 10;
            this.lblPageOnTheBoard.Text = "Page on the board: ";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new System.Drawing.Point(149, 122);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(100, 20);
            this.txtQuantity.TabIndex = 11;
            // 
            // txtRefDes
            // 
            this.txtRefDes.Location = new System.Drawing.Point(124, 28);
            this.txtRefDes.Name = "txtRefDes";
            this.txtRefDes.Size = new System.Drawing.Size(100, 20);
            this.txtRefDes.TabIndex = 12;
            // 
            // lblRId
            // 
            this.lblRId.AutoSize = true;
            this.lblRId.Location = new System.Drawing.Point(146, 72);
            this.lblRId.Name = "lblRId";
            this.lblRId.Size = new System.Drawing.Size(0, 13);
            this.lblRId.TabIndex = 13;
            // 
            // lblRCreatedBy
            // 
            this.lblRCreatedBy.AutoSize = true;
            this.lblRCreatedBy.Location = new System.Drawing.Point(146, 150);
            this.lblRCreatedBy.Name = "lblRCreatedBy";
            this.lblRCreatedBy.Size = new System.Drawing.Size(0, 13);
            this.lblRCreatedBy.TabIndex = 14;
            // 
            // lblRCreatedAt
            // 
            this.lblRCreatedAt.AutoSize = true;
            this.lblRCreatedAt.Location = new System.Drawing.Point(146, 179);
            this.lblRCreatedAt.Name = "lblRCreatedAt";
            this.lblRCreatedAt.Size = new System.Drawing.Size(0, 13);
            this.lblRCreatedAt.TabIndex = 15;
            // 
            // lblREditedBy
            // 
            this.lblREditedBy.AutoSize = true;
            this.lblREditedBy.Location = new System.Drawing.Point(146, 207);
            this.lblREditedBy.Name = "lblREditedBy";
            this.lblREditedBy.Size = new System.Drawing.Size(0, 13);
            this.lblREditedBy.TabIndex = 16;
            // 
            // lblREditedAt
            // 
            this.lblREditedAt.AutoSize = true;
            this.lblREditedAt.Location = new System.Drawing.Point(146, 233);
            this.lblREditedAt.Name = "lblREditedAt";
            this.lblREditedAt.Size = new System.Drawing.Size(0, 13);
            this.lblREditedAt.TabIndex = 17;
            // 
            // lblRPageOnTheBoard
            // 
            this.lblRPageOnTheBoard.AutoSize = true;
            this.lblRPageOnTheBoard.Location = new System.Drawing.Point(133, 56);
            this.lblRPageOnTheBoard.Name = "lblRPageOnTheBoard";
            this.lblRPageOnTheBoard.Size = new System.Drawing.Size(0, 13);
            this.lblRPageOnTheBoard.TabIndex = 18;
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(187, 398);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 19;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(106, 398);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 20;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnNextRefDes);
            this.groupBox1.Controls.Add(this.btnEditRefDes);
            this.groupBox1.Controls.Add(this.lblRefDes);
            this.groupBox1.Controls.Add(this.lblPageOnTheBoard);
            this.groupBox1.Controls.Add(this.btnPreRefDes);
            this.groupBox1.Controls.Add(this.txtRefDes);
            this.groupBox1.Controls.Add(this.lblRPageOnTheBoard);
            this.groupBox1.Location = new System.Drawing.Point(25, 262);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(237, 120);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Reference locations: ";
            // 
            // btnPreRefDes
            // 
            this.btnPreRefDes.Location = new System.Drawing.Point(12, 84);
            this.btnPreRefDes.Name = "btnPreRefDes";
            this.btnPreRefDes.Size = new System.Drawing.Size(65, 23);
            this.btnPreRefDes.TabIndex = 27;
            this.btnPreRefDes.Text = "<<";
            this.btnPreRefDes.UseVisualStyleBackColor = true;
            this.btnPreRefDes.Click += new System.EventHandler(this.btnPreRefDes_Click);
            // 
            // btnNextRefDes
            // 
            this.btnNextRefDes.Location = new System.Drawing.Point(159, 84);
            this.btnNextRefDes.Name = "btnNextRefDes";
            this.btnNextRefDes.Size = new System.Drawing.Size(65, 23);
            this.btnNextRefDes.TabIndex = 28;
            this.btnNextRefDes.Text = ">>";
            this.btnNextRefDes.UseVisualStyleBackColor = true;
            this.btnNextRefDes.Click += new System.EventHandler(this.btnNextRefDes_Click);
            // 
            // btnEditRefDes
            // 
            this.btnEditRefDes.Location = new System.Drawing.Point(81, 84);
            this.btnEditRefDes.Name = "btnEditRefDes";
            this.btnEditRefDes.Size = new System.Drawing.Size(75, 23);
            this.btnEditRefDes.TabIndex = 29;
            this.btnEditRefDes.Text = "Edit";
            this.btnEditRefDes.UseVisualStyleBackColor = true;
            this.btnEditRefDes.Click += new System.EventHandler(this.btnEditRefDes_Click);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(34, 27);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(64, 13);
            this.lblUserName.TabIndex = 27;
            this.lblUserName.Text = "User name: ";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(149, 24);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(100, 20);
            this.txtUserName.TabIndex = 28;
            // 
            // lblExternalUpdate
            // 
            this.lblExternalUpdate.AutoSize = true;
            this.lblExternalUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExternalUpdate.ForeColor = System.Drawing.Color.Green;
            this.lblExternalUpdate.Location = new System.Drawing.Point(63, 452);
            this.lblExternalUpdate.Name = "lblExternalUpdate";
            this.lblExternalUpdate.Size = new System.Drawing.Size(163, 25);
            this.lblExternalUpdate.TabIndex = 29;
            this.lblExternalUpdate.Text = "External update";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(298, 494);
            this.Controls.Add(this.lblExternalUpdate);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.lblREditedAt);
            this.Controls.Add(this.lblREditedBy);
            this.Controls.Add(this.lblRCreatedAt);
            this.Controls.Add(this.lblRCreatedBy);
            this.Controls.Add(this.lblRId);
            this.Controls.Add(this.txtQuantity);
            this.Controls.Add(this.lblEditedAt);
            this.Controls.Add(this.lblEditedBy);
            this.Controls.Add(this.lblCreatedAt);
            this.Controls.Add(this.lblCreatedBy);
            this.Controls.Add(this.lblQuantity);
            this.Controls.Add(this.lblDisctiption);
            this.Controls.Add(this.txtDiscription);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.btnBack);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.TextBox txtDiscription;
        private System.Windows.Forms.Label lblDisctiption;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Label lblCreatedBy;
        private System.Windows.Forms.Label lblCreatedAt;
        private System.Windows.Forms.Label lblEditedBy;
        private System.Windows.Forms.Label lblEditedAt;
        private System.Windows.Forms.Label lblRefDes;
        private System.Windows.Forms.Label lblPageOnTheBoard;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.TextBox txtRefDes;
        private System.Windows.Forms.Label lblRId;
        private System.Windows.Forms.Label lblRCreatedBy;
        private System.Windows.Forms.Label lblRCreatedAt;
        private System.Windows.Forms.Label lblREditedBy;
        private System.Windows.Forms.Label lblREditedAt;
        private System.Windows.Forms.Label lblRPageOnTheBoard;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnNextRefDes;
        private System.Windows.Forms.Button btnEditRefDes;
        private System.Windows.Forms.Button btnPreRefDes;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblExternalUpdate;
        private System.Windows.Forms.Timer timer1;
    }
}

